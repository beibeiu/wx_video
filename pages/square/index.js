

import { get } from '../../utils/request'

Page({

    /**
     * 页面的初始数据
     */
    data: {
        page: 1,
        isFetch: false,
        list: [
            {
                id: 1,
                time: '2020-12-12 12:00:11',
                like: 3565,
                author: '贝贝',
                cover: 'https://alifei05.cfp.cn/creative/vcg/800/new/VCG211be3c9c31.jpg',
            },
            {
                id: 2,
                like: 3565,
                author: '贝贝',
                time: '2020-12-12 12:00:11',
                cover: 'https://alifei05.cfp.cn/creative/vcg/800/new/VCG211be3c9c31.jpg',

            }, {
                id: 3,
                like: 3565,
                author: '贝贝',
                time: '2020-12-12 12:00:11',
                cover: 'https://alifei05.cfp.cn/creative/vcg/800/new/VCG211be3c9c31.jpg',

            }
        ]
    },

    // 页面加载时获取数据
    onLoad(options) {
        this.fetchData()
    },

    /**
     * 请求数据
     * @param {string} type  init|more 加载类型: 重置(刷新)|加载更多
     * @returns null
     */
    async fetchData(type = 'init') {

        if (this.data.isFetch) return console.log('正在请求中，请稍候...')

        // 为啥用 test 而不用 type === 'init' 忽略大小写判断
        // 因为可能传值的时候会有空格
        this.data.page = /init/i.test(type) ? 1 : this.data.page + 1
        this.data.isFetch = true
        //获取风格化列表
        const res = await get('/square/getSquareList', { page: this.data.page, pageSize: 10 })
        // console.log(res,this.data.list)
        this.setData({
            list: /init/i.test(type) ? res.data : this.data.list.concat(res.data)
        })
        // console.log(this.data.list)
        this.data.isFetch = false
    },

    // 下拉刷新
    onPullDownRefresh() {
        this.fetchData()
    },

    // 上拉加载更多
    onReachBottom() {
        this.fetchData('more')
    },

    // 跳转
    jump(e) {
        const id = e.currentTarget.dataset.id
        console.log("id:",id)
        wx.navigateTo({
            url: "/pages/detail/index?id="+id,
            success: function(res) {
              // 通过eventChannel向被打开页面传送数据
              res.eventChannel.emit('acceptDataFromOpenerPage', { data: 'id' })
            },
            complete(res) {
                console.log("res:",res)
            }
        })
    },

    // 页面显示
    onShow() {

    },

    // 转发给好友
    onShareAppMessage() {

    }
})