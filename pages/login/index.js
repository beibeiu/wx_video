// pages/create/index.js
import { url, get, post } from "../../utils/request"
// 获取应用实例
// const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
      userInfo: {},
      isChecked: false, // 是否勾选协议
      items: [
          { value: 'rule', name: 'rule', checked: false },
      ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },
// 判断是否选中
  checkboxChange(e) {
    // detail = {value:[选中的checkbox的value的数组]} 长度=0时没有选中
      if (e.detail.value.length) {
          this.data.isChecked = true
      } else {
          this.data.isChecked = false
      }
  },

  wxLogin() {
    //没有勾选，先勾选
      if (!this.data.isChecked) {
          return wx.showToast({
              title: '请先同意相关协议',
              icon: 'none',
              duration: 2000
          })
      }
      wx.getUserProfile({
          desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
          success: (res) => {
            console.log(res)
            this.setData({
              userInfo: res.userInfo,
            })
            wx.setStorageSync('userInfo', res.userInfo)
            wx.login({
              success: (res) => {
                console.log(res.code)
              },
            })
          },
          complete: (res) => {
              wx.setStorageSync('userInfo', res.userInfo)
              wx.navigateBack()
          }
      })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})