// pages/create/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: {
      name: '贝贝'
    },
    isLogin: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  jump() {

    wx.navigateTo({
      url: "/pages/create/index",
      complete(res) {
        console.log(res)
      }
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // 判断是否登录（本地写死）
    const userInfo = wx.getStorageSync('userInfo')

    if (!userInfo) {

      wx.navigateTo({
        url: "/pages/login/index",
      })
      return
    } else {
      this.setData({
        isLogin: true,
        user: {
          name: userInfo.nickName
        }
      })
    }

    // 请求数据


  },

  logout() {
    wx.removeStorageSync('userInfo')
    this.setData({
      isLogin: false
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})