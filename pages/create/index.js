import { url, get, post } from "../../utils/request"

const app = getApp()

// pages/create/index.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        _page: 1,

        tempVideoPath: '',


        styleData: [],
        musicData: [
            { id: 1, name: '爱情买卖', },
            { id: 2, name: '爱情买卖', }
        ],


        styleId: 0, // 选中列表第几个风格
        styleName: '',
        musicDataIndex: 0, // 选中列表第几个音乐

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

        // 通过app.globalData.userId 获取

        this.getStyle()

    },

    getStyle() {
        get('/style/getStyles').then(res => {
            this.setData({
                styleData: res.data
            })
        }).catch(err => {
            console.log('请求风格化数据出错', err)
        })
    },

    // 选择视频
    chooseMedia() {
        wx.chooseMedia({
            count: 1,
            mediaType: ['video'],
            sourceType: ['album', 'camera'],
            maxDuration: 30,
            camera: 'back',
            success: (res) => {
                console.log(res, '--选择完视频--')
                this.setData({
                    tempVideoPath: res.tempFiles[0].tempFilePath,
                })
            },
            fail: (res) => {
                console.log(res, '用户取消了选择')
            }
        })
    },

    // picker回调
    pickChange(e) {

        const index = e.detail.value
        const type = e.target.dataset.type

        // 优于 type === 'style' 判断，因为可能存在空格，校验不准
        if (/style/i.test(type)) {
            this.setData({
                styleId: this.data.styleData[index].styleId,
                styleName: this.data.styleData[index].styleName,
            })
        }

        if (/music/i.test(type)) {
            this.setData({
                musicDataIndex: index
            })
        }
    },

    send() {
        const { tempVideoPath, musicData, styleData, styleId, musicDataIndex } = this.data
        if (!tempVideoPath.trim()) return wx.showToast({
            title: '请先选择视频',
            duration: 2000
        })

        wx.showLoading({
            mask: true,
            title: '视频上传中'
        })

        wx.uploadFile({
            url: url + '/upload/uploadVideo',
            filePath: tempVideoPath,
            name: 'video',
            formData: {
                userId: app.globalData.userId,
                userName: '222',
                videoName: +new Date
            },
            success: (res) => {
                console.log(res, '上传视频返回值')
                wx.hideLoading()
                // 返回首页
                wx.navigateBack()
            },
            fail: res => {
                wx.hideLoading()
                wx.showToast({
                    icon: 'error',
                    title: '上传失败'
                })
            }
        })

    },

    preview() {
        const { styleId } = this.data
        if (!styleId) return wx.showToast({
            title: '请先选择风格化',
            duration: 2000
        })
        wx.navigateTo({
            url: '/pages/preview/index?styleId=' + styleId
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})