import { HTTPURL } from "../../utils/constant"
import { get } from '../../utils/request'
const app = getApp()
// pages/detail/index.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        id: 0,
        showPop: false, //发布评论
        inputValue: '',
        contents:[],
        likes: [],
        videoUrl: 'http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400'
    },

    //传入视频id
    onLoad(options) {

        this.data.id = options.id
        this.fetchData()
    },

    // 请求数据
    async fetchData() {
        if (this.data.isFetch) return console.log('正在请求中，请稍候...')
        this.data.isFetch = true
        const res = await get('/square/getSquareVideo', {
          squareVideoId: this.data.id
        })
        this.setData({
          videoUrl: "http://"+res.data.url
        })
        const res2 = await get('/square/getSquareVideoDetail', {
          squareVideoId: this.data.id
        })
        this.setData({
          contents: res2.data.comments
        })
  
        this.data.isFetch = false
    },

    // 分享到广场
    async shareToSquare() {
        wx.showLoading({
            mask: true,
            title: '作品分享中'
        })
        const res = await get('/square/shareVideoToSquare', {
          userId: app.globalData.userId,
          videoId: this.data.id
        })
        wx.hideLoading()
        wx.showToast({
            title: '分享到广场成功',
        })
    },

    // 保存到相册
    saveToAlbum() {
        wx.showLoading({
            mask: true,
            title: '作品下载中'
        })

        wx.downloadFile({
            url: HTTPURL + '/download',
            success: (res) => {
                wx.saveVideoToPhotosAlbum({
                    filePath: res.tempFilePath,
                    success: (res) => {
                        wx.hideLoading()
                        wx.showToast({
                            title: '保存成功',
                        })
                    },
                    fail: res => {
                        wx.hideLoading()
                        wx.showToast({
                            icon: 'error',
                            title: '保存失败',
                        })
                    }
                })
            },
            fail: res => {
                wx.hideLoading()
                wx.showToast({
                    icon: 'error',

                    title: '下载失败',
                })
            }
        })
    },
    //发布评论弹窗
    switchPop() {
        this.setData({
            showPop: !this.data.showPop
        })
    },

    onShareAppMessage() {

    }
})