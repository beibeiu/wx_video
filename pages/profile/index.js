import { url, get, post } from "../../utils/request"
const app = getApp()

// pages/profile/index.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        tempFilePath: '',
        nickName: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

        const userInfo = wx.getStorageSync('userInfo')

        if (userInfo) {

            this.setData({
                nickName: userInfo.nickName
            })


        }

    },

    changeAvatar(e) {

        // wx.chooseMedia({
        //     count: 1,
        //     mediaType: ['image'],
        //     sourceType: ['album', 'camera'],

        //     camera: 'back',
        //     success(res) {
        //         console.log(res.tempFiles[0].tempFilePath, '选中的图片')
        //     }
        // })


        wx.uploadFile({
            url: url + '/upload/uploadVideo',
            filePath: e.detail.avatarUrl,
            name: 'video',
            formData: {
                userId: app.globalData.userId,
                userName: '223332',
                videoName: +new Date
            },
            success: (res) => {
                console.log(res, '上传视频返回值')
                // wx.hideLoading()
                // 返回首页
                // wx.navigateBack()
            },
            fail: res => {
                wx.hideLoading()
                wx.showToast({
                    icon: 'error',
                    title: '上传失败'
                })
            }
        })





    },

    save() {



        wx.uploadFile({
            url: url + '/upload/uploadVideo',
            filePath: tempVideoPath,
            name: 'video',
            formData: {
                userId: app.globalData.userId,
                userName: '222',
                videoName: +new Date
            },
            success: (res) => {
                console.log(res, '上传视频返回值')
                wx.hideLoading()
                // 返回首页
                wx.navigateBack()
            },
            fail: res => {
                wx.hideLoading()
                wx.showToast({
                    icon: 'error',
                    title: '上传失败'
                })
            }
        })


        const userInfo = wx.getStorageSync('userInfo')
        userInfo.nickName = this.data.nickName.trim()
        wx.setStorageSync('userInfo', userInfo)
        wx.navigateBack()


    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})