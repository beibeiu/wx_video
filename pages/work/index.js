import { get, post } from "../../utils/request"

// pages/user/index.js
Page({

    /**
     * 页面的初始数据
     */
    data: {

        isLogin: false,

        page: 1,
        isFetch: false,
        list: [
            {
                id: 1,
                time: '2020-12-12 12:00:11',
                cover: 'https://alifei05.cfp.cn/creative/vcg/800/new/VCG211be3c9c31.jpg',
            },
            {
                id: 2,
                time: '2020-12-12 12:00:11',
                cover: 'https://alifei05.cfp.cn/creative/vcg/800/new/VCG211be3c9c31.jpg',

            }, {
                id: 3,
                time: '2020-12-12 12:00:11',
                cover: 'https://alifei05.cfp.cn/creative/vcg/800/new/VCG211be3c9c31.jpg',

            }
        ]
    },


    onLoad(options) {
        // this.fetchData()
        // 判断是否登录（本地写死）

    },


    /**
      * 请求数据
      * @param {string} type  init|more 加载类型: 重置(刷新)|加载更多
      * @returns null
      */
    async fetchData(type = 'init') {

        if (this.data.isFetch) return console.log('正在请求中，请稍候...')

        // 为啥用 test 而不用 type === 'init'
        // 因为可能传值的时候会有空格
        this.data.page = /init/i.test(type) ? 1 : this.data.page + 1
        this.data.isFetch = true
        const res = await get('/square', { page: this.data.page })
        this.setData({
            list: /init/i.test(type) ? res.list : this.data.list.concat(res.list)
        })
        this.data.isFetch = false
    },

    async delVideo(e) {
        const { id } = e.target.dataset
        wx.showModal({
            title: '提示',
            content: '确定删除该作品吗',
            success: async (res) => {
                if (res.confirm) {

                    wx.showLoading({
                        mask: true,
                        title: '删除作品中'
                    })
                    const res = await post('/del_video', { videoId: id })

                    // 更新下本地数据
                    this.data.list = this.data.list.filter(item => item.id != id)
                    this.setData({
                        list: this.data.list
                    })
                    wx.hideLoading()
                }
            }
        })
    },

    // 下拉刷新
    onPullDownRefresh() {
        this.fetchData()
    },

    // 上拉加载更多
    onReachBottom() {
        this.fetchData('more')
    },

    // 跳转
    jump(e) {
        const { id } = e.target.dataset
        wx.navigateTo({
            url: "/pages/detail/index?id=" + id,
            complete: (res) => {
                console.log(res)
            }
        })
    },



    onShow() {
        const userInfo = wx.getStorageSync('userInfo')

        if (!userInfo) {
            wx.navigateTo({
                url: "/pages/login/index",
            })
            return
        }
    },

    onShareAppMessage() {

    }
})