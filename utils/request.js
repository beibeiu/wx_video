
import { HTTPURL } from './constant'



const http = ({ url = "", params = {}, ...other } = {}) => {

    return new Promise((resolve, reject) => {
        wx.request({
            url: HTTPURL + url,
            data: params,
            ...other,
            complete: (res) => {
                if (res.statusCode >= 200 && res.statusCode < 300) {
                    resolve(res);
                } else {
                    reject(res);
                }
            },
        });

    });

};


// get方法
export function get(url, params = {}) {
    return http({
        url,
        params,
        header: { "content-type": "application/json", },
    });
}

// post方法
export function post(url, params = {}) {
    return http({
        url,
        params,
        header: { "content-type": "application/json", },
        method: "post",
    });
}

export function uploadFile(url, params = {}) {
    return http({
        header: { "content-type": "multipart/form-data", },
        method: "post",
    })
}

export const url = HTTPURL