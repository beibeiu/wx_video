

import { get } from './utils/request'


// app.js
App({
    onLaunch() {
        this.wxlogin()
    },

    wxlogin() {
        wx.login({
            success: res => {
                get('/user/login', { js_code: res.code }).then(response => {
                    this.globalData.userId = response.data.userId
                })
            }
        })
    },
    globalData: {
        userInfo: null,
        userId: ''
    }
})
